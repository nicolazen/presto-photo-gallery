from jpegtran import JPEGImage
from PIL import Image


def make_thumbnail(filename, height, width=None):
    im = JPEGImage(filename)

    try:
        im = im.exif_autotransform()
    except:
        pass

    original_height, original_width = im.height, im.width

    desired_height = int(height)
    desired_width = int(desired_height / original_height * original_width)

    if desired_width<original_width or desired_height<original_height:
        im = im.downscale(desired_width, desired_height,90)

    return im.as_blob()


def _get_image_size(filename, max_height=None):
    # keeping the PIL implementation here as it is actually surprisingly fast and also handles rotation based off EXIF tag
    return _get_image_size_and_rotation(filename, max_height)[0]


def _get_image_size_and_rotation(filename, max_height=None):
    im = _load_image(filename)
    size, seq = _rotate_size(im, im.size)
    if max_height is None:
        return size, seq
    t_h = max_height
    t_w = t_h / size[1] * size[0]
    return (t_w, t_h), seq


def _load_image(filename):
    if isinstance(filename, str):
        im = Image.open(filename)
    else:
        im = filename
    return im


def _rotate_size(im, size):
    sequence = _get_transpose_sequence(im)
    if any(op in sequence for op in [Image.ROTATE_270, Image.ROTATE_90]):
        return (size[1], size[0]), sequence
    return size, sequence


# fmt: off
# lookup for rotation of images based on EXIF tags
_exif_transpose_sequences = [                  # Val  0th row  0th col
    [],                                        #  0   (reserved)
    [],                                        #  1   top      left
    [Image.FLIP_LEFT_RIGHT],                   #  2   top      right
    [Image.ROTATE_180],                        #  3   bottom   right
    [Image.FLIP_TOP_BOTTOM],                   #  4   bottom   left
    [Image.FLIP_LEFT_RIGHT, Image.ROTATE_90],  #  5   left     top
    [Image.ROTATE_270],                        #  6   right    top
    [Image.FLIP_TOP_BOTTOM, Image.ROTATE_90],  #  7   right    bottom
    [Image.ROTATE_90],                         #  8   left     bottom
]
# fmt: on


def _get_transpose_sequence(im):
    """
        From: https://stackoverflow.com/a/30462851/7237562
        Apply Image.transpose to ensure 0th row of pixels is at the visual
        top of the image, and 0th column is the visual left-hand side.
        Return the original image if unable to determine the orientation.

        As per CIPA DC-008-2012, the orientation field contains an integer,
        1 through 8. Other values are reserved.
    """

    exif_orientation_tag = 0x0112

    try:
        return _exif_transpose_sequences[
            im._getexif()[exif_orientation_tag]  # pylint: disable=protected-access
        ]
    except Exception:  # pylint: disable=broad-except
        return []
