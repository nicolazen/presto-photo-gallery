import glob
from configuration import IMAGES_ROOT

# All lowercase, they will be matched case insensitively
IMAGE_EXTENSIONS = ["jpg", "jpeg", "png", "tiff"]
ROOT = IMAGES_ROOT.rstrip("/").rstrip("\\")
ROOT_PREFIX_SIZE = len(ROOT) + 1


def list_directories(root):
    directories = glob.glob(f"{root}*/")
    directories = [d[ROOT_PREFIX_SIZE:].replace("\\", "/") for d in directories]
    return directories


def list_images(root):
    files = glob.glob(f"{root}*.*")
    images = [f[ROOT_PREFIX_SIZE:] for f in files if _has_image_extension(f)]
    return images


def _has_image_extension(file):
    for extension in IMAGE_EXTENSIONS:
        if file.lower().endswith(extension):
            return True
    return False
