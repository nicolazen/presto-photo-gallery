import os
import urllib
import hashlib

from flask import (
    Flask,
    Blueprint,
    Response,
    abort,
    render_template,
    request,
    send_from_directory,
    url_for,
    g,
    make_response,
)

from pyinstrument import Profiler

from presto.fs import list_directories, list_images
from presto.images import make_thumbnail, _get_image_size


app = Flask(__name__)
app.config.from_object("configuration")
presto = Blueprint(  # pylint: disable=invalid-name
    "presto", __name__, template_folder="templates", static_folder="static"
)

ROOT = app.config["IMAGES_ROOT"].rstrip("/").rstrip("\\")
CACHE_PATH = app.config["CACHE_PATH"].rstrip("/").rstrip("\\")

if not os.path.exists(CACHE_PATH):
    print("...creating caching directory")
    os.makedirs(CACHE_PATH)


@app.before_request
def before_request():
    if "profile" in request.args:
        g.profiler = Profiler()
        g.profiler.start()


@app.after_request
def after_request(response):
    if not hasattr(g, "profiler"):
        return response
    g.profiler.stop()
    output_html = g.profiler.output_html()
    return make_response(output_html)


def store_thumb_cache(img_hash, image):
    path = os.path.join(CACHE_PATH, f"{img_hash}.jpg")
    with open(path, "wb") as f:
        f.write(image)


def get_thumb_from_cache(img_hash):
    path = os.path.join(CACHE_PATH, f"{img_hash}.jpg")
    with open(path, "rb") as f:
        return f.read()


def is_thumb_cached(img_hash):
    path = os.path.join(CACHE_PATH, f"{img_hash}.jpg")
    return os.path.exists(path)


def send_image(filepath):  # pylint: disable=unused-variable
    height = request.args.get("h", type=float)
    width = request.args.get("w", type=float)
    if not height:
        return send_from_directory(
            os.path.dirname(filepath), os.path.basename(filepath)
        )
    if height <= app.config["IMG_MAX_HEIGHT_PX"]:
        img_hash = hashlib.md5(f"{filepath}{height}{width}".encode()).hexdigest()
        if is_thumb_cached(img_hash):
            thumbnail = get_thumb_from_cache(img_hash)
        else:
            thumbnail = make_thumbnail(filepath, height, width)
            store_thumb_cache(img_hash, thumbnail)
    else:
        thumbnail = make_thumbnail(filepath, height, width)
    response = Response(thumbnail, mimetype="image/jpeg")
    response.cache_control.max_age = 4 * 30 * 24 * 60 * 60
    response.cache_control.public = True
    return response


def get_breadcrumbs(path):
    breadcrumbs = []
    if len(path.strip()) == 0:
        return breadcrumbs
    while path != "/":
        breadcrumbs.insert(
            0,
            (
                os.path.basename(os.path.dirname(os.path.dirname(path))),
                url_for("presto.gallery", path="/".join(path.split("/")[:-2])),
            ),
        )
        path = "/".join(path.split("/")[:-2]) + "/"
    breadcrumbs.insert(0, ("Home", url_for("presto.gallery")))
    return breadcrumbs


def send_directory_navigation(url_path, path):
    if len(path) > 0:
        if path[-1] != "/":
            path = f"{path}/"
    folders = [
        (os.path.basename(os.path.dirname(d)), d)
        for d in sorted(list_directories(path))
    ]
    images = sorted(list_images(path))
    images = get_images_json(ROOT, images)
    current_path = os.path.basename(os.path.dirname(path))
    page_name = f"{app.config['SITE_NAME']} - {current_path}"
    if current_path.strip() == "":
        current_path = app.config["SITE_NAME"]
        page_name = app.config["SITE_NAME"]
    return render_template(
        "gallery.jinja2",
        page_name=page_name,
        current_path=current_path,
        images=images,
        folders=folders,
        breadcrumbs=get_breadcrumbs(url_path),
        max_height=app.config["IMG_MAX_HEIGHT_PX"],
    )


def get_images_json(root, images):
    # FIXME: dependency on internal api, not nice
    sizes = map(_get_image_size, [f"{root}/{image}" for image in images])
    imgs = []
    for image, size in zip(images, sizes):
        image_url = urllib.parse.quote(os.path.basename(image))

        # FIXME: move this bit in the client side JS, reduced the payload and make it adapt to the viewport
        responsive_str = ", ".join(
            [
                f"{image_url}?h={round(int(a) / size[0] * size[1])} {a}"
                for a in range(250, 2750, 250)
            ]
        )
        imgs.append(
            {
                "src": os.path.basename(image),
                "width": size[0],
                "height": size[1],
                "data_responsive": responsive_str,
            }
        )
    return imgs


@presto.route("/", defaults={"path": ""})
@presto.route("/<path:path>")
def gallery(path):  # pylint: disable=unused-variable
    filepath = f"{ROOT}/{path}"

    # handle invalid request
    if not os.path.exists(filepath):
        abort(404)

    # user requested an image
    if os.path.isfile(filepath):
        return send_image(filepath)

    # otherwise user requested a folder navigation
    return send_directory_navigation(path, filepath)


app.register_blueprint(presto, url_prefix="/{}".format(app.config["BASE_URL"]))
