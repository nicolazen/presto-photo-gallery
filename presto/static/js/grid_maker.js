// This file relies on variables `images` and `maxHeight` to be defined in the
// HTML where it will be run. They are defined there as they will be injected in
// the template based on the images to be shown and the settings of Presto.

const grid = document.getElementsByClassName("grid")[0];
const observer = lozad(".lozad", {
  rootMargin: "1000px 1000px",
  threshold: 0.1,
});
let previousWidth = 0;

// First, prefill the grid with placeholders. This to determine the shape and the
// size of the grid beforehand, and then create it with the right width. The
// main problem is the scrollbar width, which has to be taken into account only
// if the content fills more than one page. I tried simpler approaches (like
// prefilling the grid with one big element of 120vh), but this is the more
// reliable.
// Moreover, I cannot fill the grid via html template, because it has to be
// sized based on the client size.
fillGrid(grid, images, true);

// now fill the grid with the actual images and kick off lazy loading
fillGrid(grid, images, false);
observer.observe();

// reflow on resize
window.addEventListener(
  "resize",
  function () {
    const desiredWidth = grid.clientWidth;
    if (desiredWidth != previousWidth) {
      fillGrid(grid, images);
      observer.observe();
    }
  },
  true
);

/**
 * Fills the grid with images
 * @param {Element} parent - element to be filled
 * @param {Array} images - elements to be displayed
 * @param {boolean} placeholders - use placeholders instead of actual images
 */
function fillGrid(parent, images, placeholders = false) {
  let currentWidth = 0;
  let rowImages = [];
  const desiredWidth = grid.clientWidth;
  previousWidth = desiredWidth;

  const new_grid = document.createElement("div");
  images.forEach((image) => {
    // very simple alogorithm. Get the displayed size of the grid container,
    // then keep on adding images until the width does not exceed the container.
    // When that happens rescale to fit the width, fill the row with the images
    // (fixing width and height to the rescaled values), and move on to the next
    // row
    rowImages.push(image);
    currentWidth += (image.width * maxHeight) / image.height;
    if (currentWidth >= desiredWidth) {
      const rowHeight = (maxHeight * desiredWidth) / currentWidth;
      fillRow(new_grid, rowImages, rowHeight, placeholders);
      currentWidth = 0;
      rowImages = [];
    }
  });
  // The leftovers element might not fill an entire row. So place them in the
  // grid assigning the max height.
  if (rowImages.length > 0) {
    fillRow(new_grid, rowImages, maxHeight, placeholders);
    currentWidth = 0;
    rowImages = [];
  }
  // swap old and new grids
  parent.innerHTML = new_grid.innerHTML;
}

/**
 * Fills a flexbox row with images
 * @param {Element} parent - element to be filled
 * @param {Array} rowPlaceholders - elements to be displayed
 * @param {Number} rowHeight - desired row height
 * @param {boolean} placeholders - use placeholders instead of actual images
 */
function fillRow(parent, rowImages, rowHeight, placeholder) {
  rowImages.forEach((rowImage) => {
    const img = document.createElement("img");
    const el = document.createElement("a");
    // the following to avoid the weird special border that appears when an img
    // tag does not have the src attribute
    if (placeholder) {
      img.setAttribute(
        "src",
        "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
      );
    } else {
      img.setAttribute("data-src", rowImage.src + "?h=" + maxHeight);
      el.setAttribute("data-responsive", rowImage.data_responsive);
    }
    img.setAttribute("height", Math.round(rowHeight) - 4);
    img.setAttribute("class", "lozad");
    img.setAttribute(
      "width",
      // Using floor here to handle a bug in Firefox, where the floats are
      // rounded up and end up overflowing the row.
      Math.floor(((rowImage.width * rowHeight) / rowImage.height - 4) * 1e1) /
        1e1
    );
    el.setAttribute("href", rowImage.src);
    el.appendChild(img);
    parent.appendChild(el);
  });
}

/**
 * Taken from https://davidwalsh.name/javascript-debounce-function
 * Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * leading edge, instead of the trailing.
 */
function debounce(func, wait, immediate) {
  let timeout;
  return function () {
    const context = this,
      args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

// Taken from https://muffinman.io/get-scrollbar-width-in-javascript/
function getScrollbarWidth() {
  return window.innerWidth - document.documentElement.clientWidth;
}
