# Presto photo gallery

<div align="center">
<img src="/docs/icon.svg" align="center" width="200" alt="Project icon">
</div>

## Aim

Serve a web gallery as simple as 1,2,3. The idea is to have a single command creating a web server that dynamically serves a tree of directories containing images. 

This we gallery space is crowded, but most of the offering either needs PHP, or a database, or is a static site. The aim here is a simple dynamic gallery, with no maintenance needed. Top notch performance is a non goal.

## Credits
Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/)
