from presto.web import app
from configuration import HOST, PORT

if __name__ == "__main__":
    app.run(host=str(HOST), port=str(PORT))
